while [[ true  ]]
do
	input=$(python ./getLine.py ttyACM2| head -1)
	if [[ $input =~ "E0E052AD" ]]
	then
		xdotool key "space"
		echo space
	elif [[ $input =~ "E0E0E01F" ]]
	then
		xdotool key "ctrl+Right"
		echo Vol up 
	elif [[ $input =~ "E0E0D02F" ]]
	then
		xdotool key "ctrl+Left"
		echo Vol down 
	elif [[ $input =~ "E0E012ED" ]]
	then
		xdotool key "Right"
		echo next 
	elif [[ $input =~ "E0E040BF" ]]
	then
		xdotool key 'ctrl+/'
		echo Power Off 
	else
		echo $input
	fi
	
done

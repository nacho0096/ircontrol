Description:
This project lets you control your Pandora station on your computer from your phone. It uses an IR sensor as a receiver for commands, the computer converts those commands into key presses that are later useful to control the music on my computer.
Installation
0)I made shortcuts to change the volume(ctrl+ left for down, ctrl+right for up)
If you have some other configuration you will need to change main.sh
1) Delete the RobotIR library from you arduino libraries(or put it somewhere else)
2)Load up the sketch IRcontrol.ino into your arduino
3)Wire the IR sensor to you arduino.
4)Install Pyserial
5)Run the main.sh script
6)Download one of the many apps that work as IR remote onto your phone.
7)Use the Samsung TV profile.
8)Go to the tab with Pandora on it.
9)Enjoy your new IR remote for your computer.

